﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace project2
{
    public class Character : Tile

    {
        public Random randal;
        private double hp = 100;
        private double totalHp = 100;
        private double attack = 1;
        private int lvl = 1;
        private double defense = 1;
        private double speed = 1;
        private double exp = 0;
        private double totalExp;
        private Item w = new Item("Fists", 0, "Bare Knuckles", "weapon", 1);
        private Item a = new Item("Rags", 0, "Torn Rags", "armor", 1);
        private List<Item> inv = new List<Item>();
        private List<char> commands = new List<char>();
        bool alive = true;
        public string name = "";
        public int currency = 0;
        public bool onEnemyTile = false;
        public bool beingChased = false;
        public bool caught = false;
        private bool onBoss;
        public int skillPoints = 0;

        public bool OnBoss
        {
            get { return onBoss; }
            set { onBoss = value; }
        }
        public int SkillPoints
        {
            get { return skillPoints; }
            set { skillPoints = value; }
        }

        public double TotalExp
        {
            get { return totalExp = (((Lvl) * Lvl) * 100) + (Lvl * 300); }
            set { totalExp = value; }
        }

        public bool Caught
        {
            get { return caught; }
            set { caught = value; }
        }
        public bool BC
        {
            get { return beingChased; }
            set { beingChased = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public bool OET
        {
            get { return onEnemyTile; }
            set { onEnemyTile = value; }
        }
        public int Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        public Item A
        {
            get { return a; }
            set { a = value; }
        }

        public Item W
        {
            get { return w; }
            set { w = value; }
        }
        public Character(string n, int lvl)
        {
            Name = n;
            Lvl = lvl;
        }

        public List<Item> Inv
        {
            get { return inv; }
            set { inv = value; }
        }
        public List<char> Commands
        {
            get { return commands; }
            set { commands = value; }
        }
        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }
        public double HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0) { Alive = false; }
                
            }
        }
        public double TotalHP
        {
            get
            {
                return totalHp;
            }
            set
            {
                totalHp = value;
            }
        }
        public double Attack
        {
            get
            {
                return attack;
            }
            set
            {
                attack = value + W.Quality;
            }
        }

        public double Defense
        {
            get
            {
                return defense;
            }

            set
            {
                defense = value + A.Quality;
            }
        }
        public double Exp
        {
            get { return exp; }
            set
            {
                exp = value;
                if (exp > TotalExp)
                {
                    LevelUp();
                }
            }
        }
        public int Lvl
        {
            get
            {
                return lvl;
            }
            set
            {
                lvl = value;
            }
        }
        public double Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public void AddItemToInv(Item i)
        {
            Inv.Add(i);
        }
        public double PAttack()
        {
            randal = new Random();

            double hit = randal.Next((int)(Attack * 2));
            hit = hit * Attack;
            return hit;
        }
        public double PBlock()
        {
            randal = new Random();

            double block = randal.Next((int)(Defense));

            return block;
        }
        public void LevelUp()
        {
            Lvl++;
            HP = TotalHP;
            TotalExp = TotalExp;
            SkillPoints += 2;
        }
    }
}
