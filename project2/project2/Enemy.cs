﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    public class Enemy : Tile
    {
        private double hp = 100;
        private double attack = 1;
        private int lvl = 1;
        private double defense = 1;
        private double speed = 1;
        bool alive = true;
        public Random randal;
        public double thp = 100;
        public Enemy()
        {

        }
        public Enemy(int l)
        {
            Lvl = l;
            HP = 100 + ((l - 1) * 10);
            Attack = ((l * 6) / 4) + l;
            Defense = ((l * 4) / 7) + l;
            Speed = l + 1;
        }
        public bool Alive
        {
            get {
                if (HP <= 0)
                {
                    Alive = false;
                }
                else Alive = true;
                return alive;
            }
            set { alive = value;}
        }
        public double HP
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0) { Alive = false; }
            }
        }
        public double TotalHP
        {
            get
            {
                return 100 + (Lvl * 10);
            }
            set
            {
                thp = 100 + (Lvl * 10);
            }
        }

        public double Attack
        {
            get
            {
                return attack;
            }
            set
            {
                attack = value;
            }
        }

        public double Defense
        {
            get
            {
                return defense;
            }

            set
            {
                defense = value;
            }
        }

        public int Lvl
        {
            get
            {
                return lvl;
            }
            set
            {
                lvl = value;
            }
        }
        public double Speed
        {
            get
            {
                return speed;
            }
            set
            {
                speed = value;
            }
        }

        public double EAttack()
        {
            randal = new Random();

            double hit = randal.Next((int)(Attack*2));
            hit = hit * Attack;
            return hit;
        }
        public double EBlock()
        {
            randal = new Random();

            double block = randal.Next((int)(Defense));

            return block;
        }
    }
}
