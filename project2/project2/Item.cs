﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    public class Item
    {
        private string iname = "";
        private double ivalue = 0.00;
        private string idescription = "";
        private string itype = "";
        private int iquality = 0;
        public Item(string n, double val, string desc, string type, int q)
        {
            Name = n;
            Value = val;
            Description = desc;
            Type = type;
            Quality = q;
        }

        public string Name
        {
            get
            {
                return iname;
            }
            set
            {
                iname = value;
            }
        }
        public int Quality
        {
            get
            {
                return iquality;
            }
            set
            {
                iquality = value;
            }
        }
        public string Type
        {
            get
            {
                return itype;
            }
            set
            {
                itype = value;
            }
        }
        public double Value
        {
            get
            {
                return ivalue;
            }
            set
            {
                ivalue = value;
            }
        }
        public string Description
        {
            get
            {
                return idescription;
            }
            set
            {
                idescription = value;
            }
        }
    }
}
