﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    public class Map
    {

        private Character c;
        private Enemy e = new Enemy();
        private Tile[,] m = new Tile[10, 10];
        private string currentMapName = "Map";

        public Map(Character ch)
        {
            C = ch;
            initializeMap();
        }

        public string CurrentMapName
        {
            get { return currentMapName; }
            set { currentMapName = value; }
        }

        public Character C
        {
            get { return c; }
            set { c = value; }
        }

        public Tile[,] M
        {
            get { return m; }
            set { m = value; }
        }

        public void SaveMap()
        {
            string[] lines = new string[100];
            int inc = 0;
            foreach (Tile t in M)
            {
                lines[inc] += t.GetSaveString();
                inc++;
            }
            File.WriteAllText(Environment.CurrentDirectory + "/Saves/" + C.Name + "_" + CurrentMapName + ".txt", String.Empty);
            System.IO.File.WriteAllLines(Environment.CurrentDirectory + "/Saves/"+C.Name+"_"+CurrentMapName+".txt", lines); 

        }

        public void LoadMap(int index)
        {
            string line;
            string[] files;
            try
            { 
                files = Directory.GetFiles(Environment.CurrentDirectory + "/Saves/");
                System.IO.StreamReader file = new System.IO.StreamReader(files[index]);
                for (int i = 0;i < 10; i++)
                {
                    for(int j = 0; i < 10; i++)
                    {
                        if((line = file.ReadLine()) != null)
                        {
                            Tile tmp = new Tile();
                            string[] tileAtt = line.Split('_');
                            tmp.X = Int32.Parse(tileAtt[0]);
                            tmp.Y = Int32.Parse(tileAtt[1]);
                            if (tileAtt[2] == "true")
                            {
                                tmp.IsBoss = true;
                            }
                            else tmp.IsBoss = false;
                            if (tileAtt[3] == "true")
                            {
                                tmp.IsEnemy = true;
                            }
                            else tmp.IsEnemy = false;
                            if (tileAtt[4] == "true")
                            {
                                tmp.IsShop = true;
                            }
                            else tmp.IsShop = false;

                            M[i, j] = tmp;
                        }
                    }
                }

                file.Close();
            }
            catch (Exception e)
            {
                
            }
        }

        public void initializeMap()
        {
            Random r = new Random();
            int k = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    k = r.Next(0, 4);
                    if (k == 1)
                    {
                        M[i, j] = new Enemy(r.Next(1, 4));
                        M[i, j].IsEnemy = true;
                        M[i, j].X = i;
                        M[i, j].Y = j;
                    }
                    else
                    {

                        M[i, j] = new Tile(i, j);

                    }

                }
            }
            //sets tile to be a shop
            M[0, 9] = new Tile(6, 0, 9);
            c.X = 0;
            c.Y = 0;
            c.IsBoss = false;
            c.IsEnemy = false;
            e = new Enemy(10);
            e.IsBoss = true;
            e.IsEnemy = true;
            e.X = 1;
            e.Y = 1;
            M[0, 0] = c;
            M[1, 1] = e;
        }
        public void FogOfWar()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    M[i, j].Visited = true;
                }
            }


        }
        public bool checkValidMove(string s)
        {
            bool legal = false;
            switch (s)
            {
                case "up":
                    if (C.X > 0)
                    {
                        legal = true;
                    }
                    break;
                case "down":
                    if (C.X < 9)
                    {
                        legal = true;
                    }
                    break;
                case "left":
                    if(C.X == 2 && C.Y == 0)
                    {
                        //SaveMap();
                        //CurrentMapName = "Castle";
                       // C.Y = 10;
                        //LoadMap(CurrentMapName);
                        legal = false;
                    }
                    if (C.Y > 0)
                    {
                        legal = true;
                    }
                    break;
                case "right":
                    if (C.X == 2 && C.Y == 9)
                    {
                        //SaveMap();
                        //CurrentMapName = "Map";
                       // C.Y = -1;
                        //LoadMap(CurrentMapName);
                        legal = false;
                    }
                    if (C.Y < 9)
                    {
                        legal = true;
                    }
                    break;
                default:
                    legal = false;
                    break;
            }
            return legal;
        }
        public bool MoveCharTile(string s)
        {

            bool moved = false;
            if (checkValidMove(s))
            {
                switch (s)
                {
                    case "up":
                        C.X--;
                        if (M[C.X, C.Y].IsEnemy)
                        {
                            if (M[C.X, C.Y].IsBoss)
                            {
                                C.OnBoss = true;
                            }
                            C.OET = true;
                        }
                        else C.OET = false;
                        moved = true;
                        break;
                    case "down":
                        C.X++;
                        if (M[C.X, C.Y].IsEnemy)
                        {
                            if (M[C.X, C.Y].IsBoss)
                            {
                                C.OnBoss = true;
                            }
                            C.OET = true;
                        }
                        else C.OET = false;
                        moved = true;
                        break;
                    case "left":
                        C.Y--;
                        if (M[C.X, C.Y].IsEnemy)
                        {
                            if (M[C.X, C.Y].IsBoss)
                            {
                                C.OnBoss = true;
                            }
                            C.OET = true;
                        }
                        else C.OET = false;
                        moved = true;
                        break;
                    case "right":
                        C.Y++;
                        if (M[C.X, C.Y].IsEnemy)
                        {
                            if (M[C.X, C.Y].IsBoss)
                            {
                                C.OnBoss = true;
                            }
                            C.OET = true;
                        }
                        else C.OET = false;
                        moved = true;
                        break;
                    default:
                        C.OET = false;
                        moved = false;
                        break;
                }
                M[C.X, C.Y].Visited = true;
            }
            C.BC = false;
            return moved;
        }

    }
}
