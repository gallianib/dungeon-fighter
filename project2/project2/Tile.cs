﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project2
{
    public class Tile
    {

        int x = 0;
        int y = 0;
        bool isenemy = false;
        bool isboss = false;
        bool isshop = false;
        bool visited = false;

        public Tile()
        {
        }
        public Tile(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Tile(int n,int x,int y)
        {
            X = x;
            Y = y;
            switch (n)
            {
                case 6:
                    IsShop = true;
                    IsBoss = false;
                    IsEnemy = false;
                    break;
                case 5:
                    IsBoss = true;
                    IsEnemy = true;
                    break;
                case 4:
                    IsBoss = false;
                    IsEnemy = false;
                    break;
                default:
                    break;
            }
        }
        public bool IsEnemy
        {
            get { return isenemy; }
            set { isenemy = value; }
        }
        public bool IsShop
        {
            get { return isshop; }
            set { isshop = value; }
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public bool IsBoss
        {
            get { return isboss; }
            set { isboss = value; }
        }
        public bool Visited
        {
            get { return visited; }
            set { visited = value; }
        }
        public string GetSaveString()
        {
            string saveString = "";
           
            saveString += this.X.ToString() + "_" + this.Y.ToString()+"_"+this.IsBoss+"_"+this.IsEnemy+"_"+this.IsShop;

            return saveString;
        }
        public string toString()
        {
            string s = "";
            if (IsEnemy && !(IsBoss) && Visited)
            {
                s = "| BOO |";
            }
            else if (IsBoss && Visited)
            {
                s = "|BOSS |";
            }
            else if (IsShop && Visited)
            {
                s = "|SHOP |";
            }
            else s = "|         |";
            return s;
        }

        
    }
}
