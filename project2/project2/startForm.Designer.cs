﻿namespace project2
{
    partial class startForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.playBox = new System.Windows.Forms.GroupBox();
            this.loadBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.statBox = new System.Windows.Forms.GroupBox();
            this.defL = new System.Windows.Forms.Label();
            this.spdL = new System.Windows.Forms.Label();
            this.atkL = new System.Windows.Forms.Label();
            this.hpL = new System.Windows.Forms.Label();
            this.skillPL = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.expL = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.miniMap = new System.Windows.Forms.Panel();
            this.mm22 = new System.Windows.Forms.Label();
            this.mm21 = new System.Windows.Forms.Label();
            this.mm20 = new System.Windows.Forms.Label();
            this.mm12 = new System.Windows.Forms.Label();
            this.mm11 = new System.Windows.Forms.Label();
            this.mm10 = new System.Windows.Forms.Label();
            this.mm02 = new System.Windows.Forms.Label();
            this.mm01 = new System.Windows.Forms.Label();
            this.mm00 = new System.Windows.Forms.Label();
            this.hpBar = new System.Windows.Forms.Label();
            this.lvlL = new System.Windows.Forms.Label();
            this.lvlLbl = new System.Windows.Forms.Label();
            this.defLbl = new System.Windows.Forms.Label();
            this.spdLbl = new System.Windows.Forms.Label();
            this.atkLbl = new System.Windows.Forms.Label();
            this.hpLbl = new System.Windows.Forms.Label();
            this.nameLbl = new System.Windows.Forms.Label();
            this.controlBox = new System.Windows.Forms.GroupBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.slot5 = new System.Windows.Forms.Label();
            this.slot4 = new System.Windows.Forms.Label();
            this.slot3 = new System.Windows.Forms.Label();
            this.slot2 = new System.Windows.Forms.Label();
            this.slot1 = new System.Windows.Forms.Label();
            this.enemyStatBox = new System.Windows.Forms.GroupBox();
            this.eHB = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.edef = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.espd = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ename = new System.Windows.Forms.Label();
            this.ehp = new System.Windows.Forms.Label();
            this.elvl = new System.Windows.Forms.Label();
            this.eatk = new System.Windows.Forms.Label();
            this.lvlUpBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.createBox = new System.Windows.Forms.GroupBox();
            this.cheatsRb = new System.Windows.Forms.CheckBox();
            this.fogRB = new System.Windows.Forms.CheckBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.enemyTimer = new System.Windows.Forms.Timer(this.components);
            this.screen = new System.Windows.Forms.GroupBox();
            this.bossImg = new System.Windows.Forms.PictureBox();
            this.enemyImg = new System.Windows.Forms.PictureBox();
            this.charImg = new System.Windows.Forms.PictureBox();
            this.loadList = new System.Windows.Forms.ListBox();
            this.playBox.SuspendLayout();
            this.statBox.SuspendLayout();
            this.miniMap.SuspendLayout();
            this.controlBox.SuspendLayout();
            this.enemyStatBox.SuspendLayout();
            this.createBox.SuspendLayout();
            this.screen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bossImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charImg)).BeginInit();
            this.SuspendLayout();
            // 
            // playBox
            // 
            this.playBox.Controls.Add(this.loadBtn);
            this.playBox.Controls.Add(this.button2);
            this.playBox.Controls.Add(this.button1);
            this.playBox.Location = new System.Drawing.Point(269, 174);
            this.playBox.Name = "playBox";
            this.playBox.Size = new System.Drawing.Size(100, 107);
            this.playBox.TabIndex = 0;
            this.playBox.TabStop = false;
            // 
            // loadBtn
            // 
            this.loadBtn.Location = new System.Drawing.Point(13, 45);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(75, 23);
            this.loadBtn.TabIndex = 2;
            this.loadBtn.Text = "Help";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Quit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Play";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statBox
            // 
            this.statBox.Controls.Add(this.defL);
            this.statBox.Controls.Add(this.spdL);
            this.statBox.Controls.Add(this.atkL);
            this.statBox.Controls.Add(this.hpL);
            this.statBox.Controls.Add(this.skillPL);
            this.statBox.Controls.Add(this.label7);
            this.statBox.Controls.Add(this.expL);
            this.statBox.Controls.Add(this.label6);
            this.statBox.Controls.Add(this.miniMap);
            this.statBox.Controls.Add(this.hpBar);
            this.statBox.Controls.Add(this.lvlL);
            this.statBox.Controls.Add(this.lvlLbl);
            this.statBox.Controls.Add(this.defLbl);
            this.statBox.Controls.Add(this.spdLbl);
            this.statBox.Controls.Add(this.atkLbl);
            this.statBox.Controls.Add(this.hpLbl);
            this.statBox.Controls.Add(this.nameLbl);
            this.statBox.Location = new System.Drawing.Point(485, 13);
            this.statBox.Name = "statBox";
            this.statBox.Size = new System.Drawing.Size(163, 393);
            this.statBox.TabIndex = 3;
            this.statBox.TabStop = false;
            this.statBox.Text = "Character Stats: ";
            this.statBox.Visible = false;
            // 
            // defL
            // 
            this.defL.AutoSize = true;
            this.defL.ForeColor = System.Drawing.SystemColors.Highlight;
            this.defL.Location = new System.Drawing.Point(104, 199);
            this.defL.Name = "defL";
            this.defL.Size = new System.Drawing.Size(22, 13);
            this.defL.TabIndex = 20;
            this.defL.Text = "0 +";
            this.defL.Click += new System.EventHandler(this.defL_Click);
            // 
            // spdL
            // 
            this.spdL.AutoSize = true;
            this.spdL.ForeColor = System.Drawing.SystemColors.Highlight;
            this.spdL.Location = new System.Drawing.Point(104, 181);
            this.spdL.Name = "spdL";
            this.spdL.Size = new System.Drawing.Size(22, 13);
            this.spdL.TabIndex = 19;
            this.spdL.Text = "0 +";
            this.spdL.Click += new System.EventHandler(this.spdL_Click);
            // 
            // atkL
            // 
            this.atkL.AutoSize = true;
            this.atkL.ForeColor = System.Drawing.SystemColors.Highlight;
            this.atkL.Location = new System.Drawing.Point(104, 161);
            this.atkL.Name = "atkL";
            this.atkL.Size = new System.Drawing.Size(22, 13);
            this.atkL.TabIndex = 18;
            this.atkL.Text = "0 +";
            this.atkL.Click += new System.EventHandler(this.atkL_Click);
            // 
            // hpL
            // 
            this.hpL.AutoSize = true;
            this.hpL.ForeColor = System.Drawing.SystemColors.Highlight;
            this.hpL.Location = new System.Drawing.Point(84, 140);
            this.hpL.Name = "hpL";
            this.hpL.Size = new System.Drawing.Size(22, 13);
            this.hpL.TabIndex = 17;
            this.hpL.Text = "0 +";
            this.hpL.Click += new System.EventHandler(this.hpL_Click);
            // 
            // skillPL
            // 
            this.skillPL.AutoSize = true;
            this.skillPL.Location = new System.Drawing.Point(126, 221);
            this.skillPL.Name = "skillPL";
            this.skillPL.Size = new System.Drawing.Size(13, 13);
            this.skillPL.TabIndex = 16;
            this.skillPL.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Skill Points Available:";
            // 
            // expL
            // 
            this.expL.AutoSize = true;
            this.expL.Location = new System.Drawing.Point(77, 84);
            this.expL.Name = "expL";
            this.expL.Size = new System.Drawing.Size(0, 13);
            this.expL.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Exp:";
            // 
            // miniMap
            // 
            this.miniMap.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.miniMap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.miniMap.Controls.Add(this.mm22);
            this.miniMap.Controls.Add(this.mm21);
            this.miniMap.Controls.Add(this.mm20);
            this.miniMap.Controls.Add(this.mm12);
            this.miniMap.Controls.Add(this.mm11);
            this.miniMap.Controls.Add(this.mm10);
            this.miniMap.Controls.Add(this.mm02);
            this.miniMap.Controls.Add(this.mm01);
            this.miniMap.Controls.Add(this.mm00);
            this.miniMap.Location = new System.Drawing.Point(6, 237);
            this.miniMap.Name = "miniMap";
            this.miniMap.Size = new System.Drawing.Size(150, 150);
            this.miniMap.TabIndex = 12;
            // 
            // mm22
            // 
            this.mm22.AutoSize = true;
            this.mm22.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm22.Location = new System.Drawing.Point(96, 100);
            this.mm22.Name = "mm22";
            this.mm22.Size = new System.Drawing.Size(0, 13);
            this.mm22.TabIndex = 8;
            // 
            // mm21
            // 
            this.mm21.AutoSize = true;
            this.mm21.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm21.Location = new System.Drawing.Point(53, 100);
            this.mm21.Name = "mm21";
            this.mm21.Size = new System.Drawing.Size(0, 13);
            this.mm21.TabIndex = 7;
            // 
            // mm20
            // 
            this.mm20.AutoSize = true;
            this.mm20.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm20.Location = new System.Drawing.Point(3, 100);
            this.mm20.Name = "mm20";
            this.mm20.Size = new System.Drawing.Size(0, 13);
            this.mm20.TabIndex = 6;
            // 
            // mm12
            // 
            this.mm12.AutoSize = true;
            this.mm12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm12.Location = new System.Drawing.Point(96, 62);
            this.mm12.Name = "mm12";
            this.mm12.Size = new System.Drawing.Size(0, 13);
            this.mm12.TabIndex = 5;
            // 
            // mm11
            // 
            this.mm11.AutoSize = true;
            this.mm11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm11.Location = new System.Drawing.Point(53, 62);
            this.mm11.Name = "mm11";
            this.mm11.Size = new System.Drawing.Size(0, 13);
            this.mm11.TabIndex = 4;
            // 
            // mm10
            // 
            this.mm10.AutoSize = true;
            this.mm10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm10.Location = new System.Drawing.Point(3, 62);
            this.mm10.Name = "mm10";
            this.mm10.Size = new System.Drawing.Size(0, 13);
            this.mm10.TabIndex = 3;
            // 
            // mm02
            // 
            this.mm02.AutoSize = true;
            this.mm02.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm02.Location = new System.Drawing.Point(96, 23);
            this.mm02.Name = "mm02";
            this.mm02.Size = new System.Drawing.Size(0, 13);
            this.mm02.TabIndex = 2;
            // 
            // mm01
            // 
            this.mm01.AutoSize = true;
            this.mm01.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm01.Location = new System.Drawing.Point(53, 23);
            this.mm01.Name = "mm01";
            this.mm01.Size = new System.Drawing.Size(0, 13);
            this.mm01.TabIndex = 1;
            // 
            // mm00
            // 
            this.mm00.AutoSize = true;
            this.mm00.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mm00.Location = new System.Drawing.Point(3, 23);
            this.mm00.Name = "mm00";
            this.mm00.Size = new System.Drawing.Size(0, 13);
            this.mm00.TabIndex = 0;
            // 
            // hpBar
            // 
            this.hpBar.AutoSize = true;
            this.hpBar.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hpBar.ForeColor = System.Drawing.Color.Maroon;
            this.hpBar.Location = new System.Drawing.Point(28, 100);
            this.hpBar.Name = "hpBar";
            this.hpBar.Size = new System.Drawing.Size(0, 34);
            this.hpBar.TabIndex = 11;
            // 
            // lvlL
            // 
            this.lvlL.AutoSize = true;
            this.lvlL.Location = new System.Drawing.Point(106, 64);
            this.lvlL.Name = "lvlL";
            this.lvlL.Size = new System.Drawing.Size(0, 13);
            this.lvlL.TabIndex = 10;
            // 
            // lvlLbl
            // 
            this.lvlLbl.AutoSize = true;
            this.lvlLbl.Location = new System.Drawing.Point(22, 64);
            this.lvlLbl.Name = "lvlLbl";
            this.lvlLbl.Size = new System.Drawing.Size(36, 13);
            this.lvlLbl.TabIndex = 9;
            this.lvlLbl.Text = "Level:";
            // 
            // defLbl
            // 
            this.defLbl.AutoSize = true;
            this.defLbl.Location = new System.Drawing.Point(22, 199);
            this.defLbl.Name = "defLbl";
            this.defLbl.Size = new System.Drawing.Size(50, 13);
            this.defLbl.TabIndex = 4;
            this.defLbl.Text = "Defense:";
            // 
            // spdLbl
            // 
            this.spdLbl.AutoSize = true;
            this.spdLbl.Location = new System.Drawing.Point(22, 181);
            this.spdLbl.Name = "spdLbl";
            this.spdLbl.Size = new System.Drawing.Size(44, 13);
            this.spdLbl.TabIndex = 3;
            this.spdLbl.Text = "Speed: ";
            // 
            // atkLbl
            // 
            this.atkLbl.AutoSize = true;
            this.atkLbl.Location = new System.Drawing.Point(22, 161);
            this.atkLbl.Name = "atkLbl";
            this.atkLbl.Size = new System.Drawing.Size(41, 13);
            this.atkLbl.TabIndex = 2;
            this.atkLbl.Text = "Attack:";
            // 
            // hpLbl
            // 
            this.hpLbl.AutoSize = true;
            this.hpLbl.Location = new System.Drawing.Point(22, 140);
            this.hpLbl.Name = "hpLbl";
            this.hpLbl.Size = new System.Drawing.Size(41, 13);
            this.hpLbl.TabIndex = 1;
            this.hpLbl.Text = "Health:";
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Font = new System.Drawing.Font("Palatino Linotype", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLbl.Location = new System.Drawing.Point(28, 29);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(78, 35);
            this.nameLbl.TabIndex = 0;
            this.nameLbl.Text = "name";
            // 
            // controlBox
            // 
            this.controlBox.Controls.Add(this.saveBtn);
            this.controlBox.Controls.Add(this.slot5);
            this.controlBox.Controls.Add(this.slot4);
            this.controlBox.Controls.Add(this.slot3);
            this.controlBox.Controls.Add(this.slot2);
            this.controlBox.Controls.Add(this.slot1);
            this.controlBox.Controls.Add(this.enemyStatBox);
            this.controlBox.Controls.Add(this.lvlUpBtn);
            this.controlBox.Controls.Add(this.button3);
            this.controlBox.Controls.Add(this.listBox1);
            this.controlBox.Location = new System.Drawing.Point(11, 412);
            this.controlBox.Name = "controlBox";
            this.controlBox.Size = new System.Drawing.Size(635, 128);
            this.controlBox.TabIndex = 2;
            this.controlBox.TabStop = false;
            this.controlBox.Visible = false;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(555, 44);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 10;
            this.saveBtn.Text = "Save Game";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Visible = false;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // slot5
            // 
            this.slot5.AutoSize = true;
            this.slot5.Location = new System.Drawing.Point(294, 93);
            this.slot5.Name = "slot5";
            this.slot5.Size = new System.Drawing.Size(27, 13);
            this.slot5.TabIndex = 9;
            this.slot5.Text = "Run";
            this.slot5.Click += new System.EventHandler(this.slot5_Click);
            // 
            // slot4
            // 
            this.slot4.AutoSize = true;
            this.slot4.Location = new System.Drawing.Point(207, 93);
            this.slot4.Name = "slot4";
            this.slot4.Size = new System.Drawing.Size(29, 13);
            this.slot4.TabIndex = 8;
            this.slot4.Text = "slot4";
            this.slot4.Visible = false;
            this.slot4.Click += new System.EventHandler(this.slot4_Click);
            // 
            // slot3
            // 
            this.slot3.AutoSize = true;
            this.slot3.Location = new System.Drawing.Point(255, 61);
            this.slot3.Name = "slot3";
            this.slot3.Size = new System.Drawing.Size(29, 13);
            this.slot3.TabIndex = 7;
            this.slot3.Text = "slot3";
            this.slot3.Visible = false;
            this.slot3.Click += new System.EventHandler(this.slot3_Click);
            // 
            // slot2
            // 
            this.slot2.AutoSize = true;
            this.slot2.Location = new System.Drawing.Point(294, 29);
            this.slot2.Name = "slot2";
            this.slot2.Size = new System.Drawing.Size(29, 13);
            this.slot2.TabIndex = 6;
            this.slot2.Text = "slot2";
            this.slot2.Visible = false;
            this.slot2.Click += new System.EventHandler(this.slot2_Click);
            // 
            // slot1
            // 
            this.slot1.AutoSize = true;
            this.slot1.Location = new System.Drawing.Point(207, 29);
            this.slot1.Name = "slot1";
            this.slot1.Size = new System.Drawing.Size(38, 13);
            this.slot1.TabIndex = 5;
            this.slot1.Text = "Attack";
            this.slot1.Click += new System.EventHandler(this.slot1_Click);
            // 
            // enemyStatBox
            // 
            this.enemyStatBox.Controls.Add(this.eHB);
            this.enemyStatBox.Controls.Add(this.label5);
            this.enemyStatBox.Controls.Add(this.edef);
            this.enemyStatBox.Controls.Add(this.label4);
            this.enemyStatBox.Controls.Add(this.espd);
            this.enemyStatBox.Controls.Add(this.label3);
            this.enemyStatBox.Controls.Add(this.label2);
            this.enemyStatBox.Controls.Add(this.ename);
            this.enemyStatBox.Controls.Add(this.ehp);
            this.enemyStatBox.Controls.Add(this.elvl);
            this.enemyStatBox.Controls.Add(this.eatk);
            this.enemyStatBox.Location = new System.Drawing.Point(364, 1);
            this.enemyStatBox.Name = "enemyStatBox";
            this.enemyStatBox.Size = new System.Drawing.Size(184, 127);
            this.enemyStatBox.TabIndex = 4;
            this.enemyStatBox.TabStop = false;
            this.enemyStatBox.Text = "Enemy Stats: ";
            this.enemyStatBox.Visible = false;
            // 
            // eHB
            // 
            this.eHB.AutoSize = true;
            this.eHB.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eHB.ForeColor = System.Drawing.Color.Maroon;
            this.eHB.Location = new System.Drawing.Point(97, 43);
            this.eHB.Name = "eHB";
            this.eHB.Size = new System.Drawing.Size(69, 20);
            this.eHB.TabIndex = 12;
            this.eHB.Text = "----------";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(91, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Defense:";
            // 
            // edef
            // 
            this.edef.AutoSize = true;
            this.edef.Location = new System.Drawing.Point(141, 69);
            this.edef.Name = "edef";
            this.edef.Size = new System.Drawing.Size(28, 13);
            this.edef.TabIndex = 8;
            this.edef.Text = "edef";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Speed: ";
            // 
            // espd
            // 
            this.espd.AutoSize = true;
            this.espd.Location = new System.Drawing.Point(56, 69);
            this.espd.Name = "espd";
            this.espd.Size = new System.Drawing.Size(30, 13);
            this.espd.TabIndex = 6;
            this.espd.Text = "espd";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Health:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Attack: ";
            // 
            // ename
            // 
            this.ename.AutoSize = true;
            this.ename.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ename.Location = new System.Drawing.Point(11, 16);
            this.ename.Name = "ename";
            this.ename.Size = new System.Drawing.Size(58, 25);
            this.ename.TabIndex = 3;
            this.ename.Text = "name";
            // 
            // ehp
            // 
            this.ehp.AutoSize = true;
            this.ehp.Location = new System.Drawing.Point(56, 49);
            this.ehp.Name = "ehp";
            this.ehp.Size = new System.Drawing.Size(25, 13);
            this.ehp.TabIndex = 2;
            this.ehp.Text = "ehp";
            // 
            // elvl
            // 
            this.elvl.AutoSize = true;
            this.elvl.Location = new System.Drawing.Point(98, 25);
            this.elvl.Name = "elvl";
            this.elvl.Size = new System.Drawing.Size(23, 13);
            this.elvl.TabIndex = 1;
            this.elvl.Text = "elvl";
            // 
            // eatk
            // 
            this.eatk.AutoSize = true;
            this.eatk.Location = new System.Drawing.Point(56, 92);
            this.eatk.Name = "eatk";
            this.eatk.Size = new System.Drawing.Size(28, 13);
            this.eatk.TabIndex = 0;
            this.eatk.Text = "eatk";
            // 
            // lvlUpBtn
            // 
            this.lvlUpBtn.Location = new System.Drawing.Point(554, 70);
            this.lvlUpBtn.Name = "lvlUpBtn";
            this.lvlUpBtn.Size = new System.Drawing.Size(75, 23);
            this.lvlUpBtn.TabIndex = 3;
            this.lvlUpBtn.Text = "Level Up";
            this.lvlUpBtn.UseVisualStyleBackColor = true;
            this.lvlUpBtn.Visible = false;
            this.lvlUpBtn.Click += new System.EventHandler(this.lvlUpBtn_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(554, 99);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Quit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(7, 14);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(177, 108);
            this.listBox1.TabIndex = 0;
            // 
            // createBox
            // 
            this.createBox.Controls.Add(this.cheatsRb);
            this.createBox.Controls.Add(this.fogRB);
            this.createBox.Controls.Add(this.startBtn);
            this.createBox.Controls.Add(this.nameInput);
            this.createBox.Controls.Add(this.label1);
            this.createBox.Location = new System.Drawing.Point(2, 220);
            this.createBox.Name = "createBox";
            this.createBox.Size = new System.Drawing.Size(229, 218);
            this.createBox.TabIndex = 3;
            this.createBox.TabStop = false;
            this.createBox.Text = "Create Character: ";
            this.createBox.Visible = false;
            // 
            // cheatsRb
            // 
            this.cheatsRb.AutoSize = true;
            this.cheatsRb.Location = new System.Drawing.Point(77, 113);
            this.cheatsRb.Name = "cheatsRb";
            this.cheatsRb.Size = new System.Drawing.Size(95, 17);
            this.cheatsRb.TabIndex = 5;
            this.cheatsRb.Text = "Enable Cheats";
            this.cheatsRb.UseVisualStyleBackColor = true;
            // 
            // fogRB
            // 
            this.fogRB.AutoSize = true;
            this.fogRB.Location = new System.Drawing.Point(77, 89);
            this.fogRB.Name = "fogRB";
            this.fogRB.Size = new System.Drawing.Size(117, 17);
            this.fogRB.TabIndex = 4;
            this.fogRB.Text = "Disable Fog of War";
            this.fogRB.UseVisualStyleBackColor = true;
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(77, 162);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(75, 23);
            this.startBtn.TabIndex = 3;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // nameInput
            // 
            this.nameInput.Location = new System.Drawing.Point(77, 41);
            this.nameInput.MaxLength = 10;
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(100, 20);
            this.nameInput.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name: ";
            // 
            // enemyTimer
            // 
            this.enemyTimer.Interval = 35;
            this.enemyTimer.Tick += new System.EventHandler(this.enemyTimer_Tick);
            // 
            // screen
            // 
            this.screen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.screen.Controls.Add(this.bossImg);
            this.screen.Controls.Add(this.enemyImg);
            this.screen.Controls.Add(this.charImg);
            this.screen.Location = new System.Drawing.Point(12, 12);
            this.screen.Name = "screen";
            this.screen.Size = new System.Drawing.Size(467, 394);
            this.screen.TabIndex = 2;
            this.screen.TabStop = false;
            this.screen.Visible = false;
            this.screen.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.screen_KeyDown);
            // 
            // bossImg
            // 
            this.bossImg.BackColor = System.Drawing.Color.Transparent;
            this.bossImg.Image = global::project2.Properties.Resources.bowser;
            this.bossImg.Location = new System.Drawing.Point(350, 250);
            this.bossImg.Name = "bossImg";
            this.bossImg.Size = new System.Drawing.Size(100, 120);
            this.bossImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bossImg.TabIndex = 2;
            this.bossImg.TabStop = false;
            // 
            // enemyImg
            // 
            this.enemyImg.BackColor = System.Drawing.Color.Transparent;
            this.enemyImg.Image = global::project2.Properties.Resources.boo;
            this.enemyImg.Location = new System.Drawing.Point(350, 250);
            this.enemyImg.Name = "enemyImg";
            this.enemyImg.Size = new System.Drawing.Size(62, 50);
            this.enemyImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.enemyImg.TabIndex = 1;
            this.enemyImg.TabStop = false;
            // 
            // charImg
            // 
            this.charImg.BackColor = System.Drawing.Color.Transparent;
            this.charImg.Image = global::project2.Properties.Resources.mario1;
            this.charImg.Location = new System.Drawing.Point(6, 15);
            this.charImg.Name = "charImg";
            this.charImg.Size = new System.Drawing.Size(50, 50);
            this.charImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.charImg.TabIndex = 0;
            this.charImg.TabStop = false;
            // 
            // loadList
            // 
            this.loadList.FormattingEnabled = true;
            this.loadList.Location = new System.Drawing.Point(2, 77);
            this.loadList.Name = "loadList";
            this.loadList.Size = new System.Drawing.Size(533, 95);
            this.loadList.TabIndex = 4;
            this.loadList.Visible = false;
            this.loadList.SelectedIndexChanged += new System.EventHandler(this.loadList_SelectedIndexChanged);
            // 
            // startForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(659, 549);
            this.Controls.Add(this.loadList);
            this.Controls.Add(this.createBox);
            this.Controls.Add(this.controlBox);
            this.Controls.Add(this.statBox);
            this.Controls.Add(this.playBox);
            this.Controls.Add(this.screen);
            this.Name = "startForm";
            this.Text = "Boop";
            this.Load += new System.EventHandler(this.startForm_Load);
            this.playBox.ResumeLayout(false);
            this.statBox.ResumeLayout(false);
            this.statBox.PerformLayout();
            this.miniMap.ResumeLayout(false);
            this.miniMap.PerformLayout();
            this.controlBox.ResumeLayout(false);
            this.controlBox.PerformLayout();
            this.enemyStatBox.ResumeLayout(false);
            this.enemyStatBox.PerformLayout();
            this.createBox.ResumeLayout(false);
            this.createBox.PerformLayout();
            this.screen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bossImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enemyImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox playBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox screen;
        private System.Windows.Forms.GroupBox statBox;
        private System.Windows.Forms.GroupBox controlBox;
        private System.Windows.Forms.Label defLbl;
        private System.Windows.Forms.Label spdLbl;
        private System.Windows.Forms.Label atkLbl;
        private System.Windows.Forms.Label hpLbl;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label lvlL;
        private System.Windows.Forms.Label lvlLbl;
        private System.Windows.Forms.Label hpBar;
        private System.Windows.Forms.Panel miniMap;
        private System.Windows.Forms.Label mm22;
        private System.Windows.Forms.Label mm21;
        private System.Windows.Forms.Label mm20;
        private System.Windows.Forms.Label mm12;
        private System.Windows.Forms.Label mm11;
        private System.Windows.Forms.Label mm10;
        private System.Windows.Forms.Label mm02;
        private System.Windows.Forms.Label mm01;
        private System.Windows.Forms.Label mm00;
        private System.Windows.Forms.PictureBox charImg;
        private System.Windows.Forms.PictureBox bossImg;
        private System.Windows.Forms.PictureBox enemyImg;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button lvlUpBtn;
        private System.Windows.Forms.GroupBox createBox;
        private System.Windows.Forms.CheckBox cheatsRb;
        private System.Windows.Forms.CheckBox fogRB;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox enemyStatBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label edef;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label espd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ename;
        private System.Windows.Forms.Label ehp;
        private System.Windows.Forms.Label elvl;
        private System.Windows.Forms.Label eatk;
        private System.Windows.Forms.Label eHB;
        private System.Windows.Forms.Timer enemyTimer;
        private System.Windows.Forms.Label expL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label skillPL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label defL;
        private System.Windows.Forms.Label spdL;
        private System.Windows.Forms.Label atkL;
        private System.Windows.Forms.Label hpL;
        private System.Windows.Forms.Label slot5;
        private System.Windows.Forms.Label slot4;
        private System.Windows.Forms.Label slot3;
        private System.Windows.Forms.Label slot2;
        private System.Windows.Forms.Label slot1;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.ListBox loadList;
    }
}