﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace project2
{
    public partial class startForm : Form
    {
        Map map;
        Enemy CurrentEnemy;
        bool engaged = false;
        bool playerTurn = false;
        bool enemyTurn = false;

        public startForm()
        {
            InitializeComponent();
            
        }
        private void startForm_Load(object sender, EventArgs e)
        {
            playBox.Visible = true;
            controlBox.Visible = false;
            statBox.Visible = false;
            screen.Visible = false;
            createBox.Visible = false;
            enemyStatBox.Visible = false;
        }

        public void button1_Click(object sender, EventArgs e)
        {

            playBox.Visible = false;
            createBox.Visible = true;
            createBox.Location = new Point(210, 145);
            

        }

        public void GetMap(int x, int y)
        {
            string X = x.ToString();
            string Y = y.ToString();
            Image bgImage;
            try
            {
                bgImage = new Bitmap(Environment.CurrentDirectory +"/"+ map.CurrentMapName + "/"+ X + "_" + Y + ".jpg");
            }
            catch (Exception e)
            {
                bgImage = new Bitmap(Environment.CurrentDirectory + "/Map/null.jpg");
            }

            screen.BackgroundImage = bgImage;

        }

        public void button2_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().CloseMainWindow();
            
        }


        public void startBtn_Click(object sender, EventArgs e)
        {
            string n = nameInput.Text;
            Character cha = new Character(n, 1);

            map = new Map(cha);
            GetMap(0, 0);
            if (fogRB.Checked)
            {
                map.FogOfWar();
            }
            if (cheatsRb.Checked)
            {
                lvlUpBtn.Visible = true;
            }
            playBox.Visible = false;
            createBox.Visible = false;
            statBox.Visible = true;
            screen.Visible = true;
            controlBox.Visible = true;
            enemyImg.Visible = false;
            bossImg.Visible = false;
            DisplayStats(map);
            DisplayMiniMap(map);
            screen.Focus();

        }
        public void DisplayMiniMap(Map map)
        {
            mm00.Text = "";
            mm01.Text = "";
            mm02.Text = "";
            mm10.Text = "";
            mm12.Text = "";
            mm20.Text = "";
            mm21.Text = "";
            mm22.Text = "";
            mm11.Text = "| YOU |";
            if (map.C.X > 0 && map.C.Y > 0 && map.C.X < 9 && map.C.Y < 9)
            {
                mm00.Text = map.M[map.C.X - 1, map.C.Y - 1].toString();
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm02.Text = map.M[map.C.X - 1, map.C.Y + 1].toString();
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
                mm20.Text = map.M[map.C.X + 1, map.C.Y - 1].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
                mm22.Text = map.M[map.C.X + 1, map.C.Y + 1].toString();
            }
            else if (map.C.X == 0 && map.C.Y == 0)
            {
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
                mm22.Text = map.M[map.C.X + 1, map.C.Y + 1].toString();
            }
            else if (map.C.X == 0 && map.C.Y > 0 && map.C.Y < 9)
            {
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
                mm20.Text = map.M[map.C.X + 1, map.C.Y - 1].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
                mm22.Text = map.M[map.C.X + 1, map.C.Y + 1].toString();
            }
            else if (map.C.X == 0 && map.C.Y == 9)
            {
                mm20.Text = map.M[map.C.X + 1, map.C.Y - 1].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
            }
            else if (map.C.X > 0 && map.C.Y == 0 && map.C.X < 9)
            {
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
                mm22.Text = map.M[map.C.X + 1, map.C.Y + 1].toString();
                mm02.Text = map.M[map.C.X - 1, map.C.Y + 1].toString();
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
            }
            else if (map.C.X == 9 && map.C.Y == 0)
            {
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm02.Text = map.M[map.C.X - 1, map.C.Y + 1].toString();
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
            }
            else if (map.C.X == 9 && map.C.Y > 0 && map.C.Y < 9)
            {
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
                mm12.Text = map.M[map.C.X, map.C.Y + 1].toString();
                mm00.Text = map.M[map.C.X - 1, map.C.Y - 1].toString();
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm02.Text = map.M[map.C.X - 1, map.C.Y + 1].toString();
            }
            else if (map.C.X == 9 && map.C.Y == 9)
            {
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
                mm00.Text = map.M[map.C.X - 1, map.C.Y - 1].toString();
            }
            else if (map.C.X > 0 && map.C.X < 9 && map.C.Y == 9)
            {
                mm01.Text = map.M[map.C.X - 1, map.C.Y].toString();
                mm10.Text = map.M[map.C.X, map.C.Y - 1].toString();
                mm00.Text = map.M[map.C.X - 1, map.C.Y - 1].toString();
                mm20.Text = map.M[map.C.X + 1, map.C.Y - 1].toString();
                mm21.Text = map.M[map.C.X + 1, map.C.Y].toString();
            }

        }
        
        public void DisplayStats(Map map)
        {
            hpBar.Text = "";
            double p = map.C.TotalHP;
            nameLbl.Text = map.C.Name;
            lvlL.Text = string.Format("{0:0}", map.C.Lvl, map.C.Exp%100);
            expL.Text = string.Format("{0:0}/{1:0}", map.C.Exp, map.C.TotalExp);
            
            for (int i = 1; i <= 10; i++)
            {
                if (map.C.HP >= ((p * 0.1) * i))
                {
                    hpBar.Text += "-";
                }

            }
            if (map.C.SkillPoints > 0)
            {
                hpL.Text = string.Format("{0:0}/{1:0} +", map.C.HP, map.C.TotalHP);
                atkL.Text = string.Format("{0:0} +", map.C.Attack);
                spdL.Text = string.Format("{0:0} +", map.C.Speed);
                defL.Text = string.Format("{0:0} +", map.C.Defense);
            }
            else
            {
                hpL.Text = string.Format("{0:0}/{1:0}", map.C.HP,map.C.TotalHP);
                atkL.Text = string.Format("{0:0}", map.C.Attack);
                spdL.Text = string.Format("{0:0}", map.C.Speed);
                defL.Text = string.Format("{0:0}", map.C.Defense);
            }
            skillPL.Text = string.Format("{0:0}", map.C.SkillPoints);
        }
        public void DisplayEnemyStats(Map map)
        {
            eHB.Text = "";
            if (map.M[map.C.X, map.C.Y].IsEnemy)
            {
                enemyStatBox.Visible = true;
          
                Enemy E = (Enemy)map.M[map.C.X, map.C.Y];
                if (map.M[map.C.X, map.C.Y].IsBoss)
                {
                    ename.Text = "Bowser";
                }
                else
                {
                    ename.Text = "Boo";
                }
                
                elvl.Text = string.Format("{0:0}", E.Lvl);
                ehp.Text = string.Format("{0:0}",E.HP);
                for (int i = 1; i <= 10; i++)
                {
                    if (E.HP >= ((E.TotalHP * 0.1) * i))
                    {
                        eHB.Text += "-";
                    }

                }
                eatk.Text = string.Format("{0:0}", E.Attack);
                espd.Text = string.Format("{0:0}",E.Speed);
                edef.Text = string.Format("{0:0}",E.Defense);
                if (E.HP <= 0)
                {
                    //death
                    KillFight(E);

                    map.M[map.C.X, map.C.Y] = new Tile();
                }
            }
            else
            {
                enemyStatBox.Visible = false;
            }
            
        }
        private void screen_KeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            int x = charImg.Location.X;
            int y = charImg.Location.Y;
            bool m = false;
            if (map.C.Alive && !engaged)
            {
                switch (e.KeyCode)
                {
                    case Keys.D:
                        x += (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario1;
                        break;
                    case Keys.Right:
                        x += (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario1;
                        break;

                    case Keys.A:
                        x -= (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario2;
                        break;
                    case Keys.Left:
                        x -= (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario2;
                        break;

                    case Keys.W:
                        y -= (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario1;
                        break;
                    case Keys.Up:
                        y -= (int)(2 * map.C.Speed) + 3;
                        charImg.Image = project2.Properties.Resources.mario1;
                        break;

                    case Keys.S:
                        y += (int)(2 * map.C.Speed) + 3;
                        break;
                    case Keys.Down:
                        y += (int)(2 * map.C.Speed) + 3;
                        break;


                }
             }

                if (x > 440)
                {
                    if (map.MoveCharTile("right"))
                    {
                        m = true;
                        x = 5;
                    }
                    else { x = 440; }
                }
                if (x < 5)
                {
                    if (map.MoveCharTile("left"))
                    {
                        m = true;
                        x = 435;
                    }
                    else { x = 5; }
                }
                if (y > 370)
                {
                    if (map.MoveCharTile("down"))
                    {
                        m = true;
                        y = 9;
                    }
                    else { y = 370; }
                }
                if (y < 9)
                {
                    if (map.MoveCharTile("up"))
                    {
                        m = true;
                        y = 365;
                    }
                    else { y = 9; }
                }
            
            charImg.Location = new Point(x, y);
            if (m)
            {
                GetMap(map.C.X, map.C.Y);
                DisplayMiniMap(map);
                DisplayStats(map);

                if (map.M[map.C.X, map.C.Y].IsEnemy && !map.M[map.C.X, map.C.Y].IsBoss)
                {
                    DisplayEnemyStats(map);
                    listBox1.Items.Clear();
                    listBox1.Items.Add("You Ran into a Boo!");
                    enemyImg.Visible = true;
                    bossImg.Visible = false;
                    enemyTimer.Enabled = true;
                }
                else if (map.M[map.C.X,map.C.Y].IsBoss)
                {
                    DisplayEnemyStats(map);
                    listBox1.Items.Clear();
                    listBox1.Items.Add("It's Bowser!!!");
                    bossImg.Visible = true;
                    enemyImg.Visible = false;
                    enemyTimer.Enabled = true;
                }
                else if (map.M[map.C.X, map.C.Y].IsShop)
                {
                    enemyStatBox.Visible = false;
                    listBox1.Items.Clear();
                    enemyImg.Visible = false;
                    bossImg.Visible = false;
                    enemyTimer.Enabled = false;
                    listBox1.Items.Add("Welcome to the Shop!");
                    openShop(map);
                }
                else
                {
                    enemyStatBox.Visible = false;
                    listBox1.Items.Clear();
                    enemyImg.Visible = false;
                    bossImg.Visible = false;
                    enemyTimer.Enabled = false;
                }
            } 
        }

        
        public bool Touching(PictureBox c,PictureBox e)
        {
            int offset = 9;
            bool touching = false;
            int cx = c.Location.X;
            int cy = c.Location.Y;
            int ex = e.Location.X;
            int ey = e.Location.Y;
            if ((ex + offset <= (cx + c.Width)-offset && ex+offset >= cx + offset) && (ey+offset <= (c.Height + cy)-offset && ey+offset >= cy-offset))
            {
                touching = true;
            }
            if ((ex+e.Width-offset <= (cx + c.Width)-offset && ex+e.Width-offset >= cx+offset) && (ey+offset <= (c.Height + cy)-offset && ey+offset >= cy+offset))
            {
                touching = true;
            }
            if ((ex+offset <= (cx + c.Width)-offset && ex+offset >= cx+offset) && ((ey+e.Height)-offset <= (c.Height + cy)-offset && (ey+e.Height)-offset >= cy+offset))
            {
                touching = true;
            }
            if (((ex+e.Width)-offset <= (cx + c.Width)-offset && (ex+e.Width)-offset >= cx+offset) && ((ey+e.Height)-offset <= (c.Height + cy)-offset && (ey+e.Height)-offset >= cy+offset))
            {
                touching = true;
            }
            return touching;
        }
        
        public void openShop(Map map)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void lvlUpBtn_Click(object sender, EventArgs e)
        {
            map.C.Exp += 1000;
            DisplayStats(map);
            screen.Focus();
        }

        private void enemyTimer_Tick(object sender, EventArgs e)
        {
            Point playerC = new Point(charImg.Location.X, charImg.Location.Y);
            Point enemyC;
            int b = 0;
            bool touching = false;
            
            if (map.M[map.C.X,map.C.Y].IsBoss)
            {
                b = 1;
                enemyC = new Point(bossImg.Location.X, bossImg.Location.Y);
                if (playerC.X < enemyC.X)
                {
                    enemyC.X -= 8;
                }
                else if (playerC.X > enemyC.X)
                {
                    enemyC.X += 8;
                }
                if (playerC.Y < enemyC.Y)
                {
                    enemyC.Y -= 8;
                }
                else if (playerC.Y > enemyC.Y)
                {
                    enemyC.Y += 8;
                }
                bossImg.Location = enemyC;
                touching = Touching(charImg, bossImg);
            }
            else 
            {
                b = 0;
                enemyC = new Point(enemyImg.Location.X, enemyImg.Location.Y);
                if (playerC.X < enemyC.X)
                {
                    enemyC.X -= 6;
                }
                else if (playerC.X > enemyC.X)
                {
                    enemyC.X += 6;
                }
                if (playerC.Y < enemyC.Y)
                {
                    enemyC.Y -= 6;
                }
                else if (playerC.Y > enemyC.Y)
                {
                    enemyC.Y += 6;
                }
                enemyImg.Location = enemyC;
                touching = Touching(charImg, enemyImg);
            }

            if (touching)
            {
                if (enemyImg.Visible == true || bossImg.Visible == true) {
                    enemyTimer.Enabled = false;
                    engaged = true;
                    BeginFight(b, map.C, (Enemy)map.M[map.C.X, map.C.Y]);
                }
            }
            

        }
        public void BeginFight(int b,Character c,Enemy e)
        {
            Point playerSet = new Point(20,280);
            Point enemySet = new Point(350,25);
            charImg.Location = playerSet;
            if (b == 1) bossImg.Location = enemySet;
            else enemyImg.Location = enemySet;
            
            listBox1.Items.Clear();
            if (b == 1) listBox1.Items.Add("Engaged Bowser!");
            else listBox1.Items.Add("Engaged An Enemy!");

            controlBox.Focus();

            CurrentEnemy = e;
            playerTurn = true;
            enemyTurn = false;


           
            
        }


        private void defL_Click(object sender, EventArgs e)
        {
            if(map.C.SkillPoints > 0)
            {
                map.C.Defense++;
                map.C.SkillPoints--;
            }
            DisplayStats(map);
        }

        private void spdL_Click(object sender, EventArgs e)
        {
            if (map.C.SkillPoints > 0)
            {
                map.C.Speed++;
                map.C.SkillPoints--;
                
            }
            DisplayStats(map);
        }

        private void atkL_Click(object sender, EventArgs e)
        {
            if (map.C.SkillPoints > 0)
            {
                map.C.Attack++;
                map.C.SkillPoints--;
                
            }
            DisplayStats(map);
        }

        private void hpL_Click(object sender, EventArgs e)
        {
            if (map.C.SkillPoints > 0)
            {
                map.C.TotalHP+= 10;
                map.C.SkillPoints--;
                
            }
            DisplayStats(map);
        }

        private void slot1_Click(object sender, EventArgs e)
        {
            if (playerTurn && !enemyTurn)
            {
                UseSlot(slot1.Text);
            }
            else listBox1.Items.Add("It is not your move!");
        }

        private void slot2_Click(object sender, EventArgs e)
        {
            if (playerTurn && !enemyTurn)
            {
                UseSlot(slot2.Text);
            }
            else listBox1.Items.Add("It is not your move!");
        }

        private void slot3_Click(object sender, EventArgs e)
        {
            if (playerTurn && !enemyTurn)
            {
                UseSlot(slot3.Text);
            }
            else listBox1.Items.Add("It is not your move!");
        }

        private void slot4_Click(object sender, EventArgs e)
        {
            if (playerTurn && !enemyTurn)
            {
                UseSlot(slot4.Text);
            }
            else listBox1.Items.Add("It is not your move!");
        }

        private void slot5_Click(object sender, EventArgs e)
        {
            if (playerTurn && !enemyTurn)
            {
                UseSlot(slot5.Text);
            }
            else listBox1.Items.Add("It is not your move!");
        }

        public void EndFight()
        {
            playerTurn = false;
            enemyTurn = false;
            engaged = false;
            bossImg.Location = new Point(350,250);
            enemyImg.Location = new Point(350, 250);
            screen.Focus();
            enemyTimer.Enabled = true;
        }
        public void KillFight(Enemy E)
        {
            playerTurn = false;
            enemyTurn = false;
            engaged = false;
            bossImg.Visible = false;
            enemyImg.Visible = false;
            screen.Focus();
            enemyTimer.Enabled = false;
            map.C.Exp += 100 * E.Lvl;
        }

        public void UseSlot(string n)
        {
            listBox1.Items.Add("Used "+n);
            switch (n)
            {
                case "Attack":
                    CurrentEnemy.HP -= 100;
                    break;
                case "2":
                    break;
                case "3":
                    break;
                case "4":
                    break;
                case "Run":
                    EndFight();
                    break;
            }
            playerTurn = false;
            enemyTurn = true;
            DisplayEnemyStats(map);
            DisplayStats(map);
            TakeEnemyTurn();
        }
        public void TakeEnemyTurn()
        {
            enemyTurn = false;
            playerTurn = true;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            map.SaveMap();
        }

        private void loadBtn_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Controls: \n\tMove: (W,A,S,D) or Arrow Keys\n\n Your Characters stats are on the right side of screen,\n when you level up, click on the stats you want to improve.\n Fight Boos to gain experience and Defeat the Boss to win!");

            /*loadList.Items.Clear();
            string[] files;
            files = Directory.GetFiles(Environment.CurrentDirectory + "/Saves/");
                for (int i = 0; i < files.Length; i++)
            {
                string[] f;
                f = files[i].Split('/');
                loadList.Items.Add(files[i].ToString());
            }
            loadList.Visible = true;*/

        }

        private void loadList_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadList.Visible = false;
            string curItem = loadList.SelectedItem.ToString();
            int index = loadList.FindString(curItem);
            //MessageBox.Show(curItem + " - " + index);
            map.LoadMap(index);
        }
    } 
}
